# OpenML dataset: Heart-Disease

https://www.openml.org/d/43398

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This database contains 76 attributes, but all published experiments refer to using a subset of 14 of them. In particular, the Cleveland database is the only one that has been used by ML researchers to
this date. The "goal" field refers to the presence of heart disease in the patient. It is integer-valued from 0 (no presence) to 4.
Acknowledgements
Creators:
Hungarian Institute of Cardiology. Budapest: Andras Janosi, M.D.
University Hospital, Zurich, Switzerland: William Steinbrunn, M.D.
University Hospital, Basel, Switzerland: Matthias Pfisterer, M.D.
V.A. Medical Center, Long Beach and Cleveland Clinic Foundation: Robert Detrano, M.D., PhD.
Donor:
David W. Aha (aha '' ics.uci.edu) (714) 856-8779
Inspiration
Experiments with the Cleveland database have concentrated on simply attempting to distinguish presence (values 1,2,3,4) from absence (value 0).
See if you can find any other trends in heart data to predict certain cardiovascular events or find any clear indications of heart health.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43398) of an [OpenML dataset](https://www.openml.org/d/43398). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43398/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43398/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43398/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

